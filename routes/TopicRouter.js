const express = require("express");
const connect = require("../db");

const TopicRouter = express.Router();

TopicRouter.route("/").get((req, res) => {
    connect.query('SELECT * FROM Topic', (error, result) => {
        res.send(result);
    });
    //Création d'un topic.
}).post((req, res) => {
    connect.query('INSERT INTO Topic (title) VALUES (?)',
    [req.body.title],
    (error, result) => {
        if(error) res.status(400).send(error);
        else res.send("Créé.")
    });
});

TopicRouter.route("/:id").get((req, res) => {
    const idParam = req.params.id;
    if(!idParam) res.status(400).send("Mauvais identiant.")
    else connect.query('SELECT * FROM Topic WHERE id = ? LIMIT 1', [idParam], (error, results) => {
        if(error) res.status(400).send(error);
        else res.send(results[0]);
    });
    //Modificaztion d'un topic.
}).put((req, res) => {
    const idParam = req.params.id;
    if(!idParam) res.status(400).send("Mauvais identiant.")
    else {
        con.query(`UPDATE Topic SET title=? WHERE id=?`,
        [req.body.title, idParam], (error, result) => {
            if(error) res.status(400).send(error);
            else res.send(result.message)
        });
    }
// Supression d'un topic.
}).delete((req, res) => {
    const idParam = req.params.id;
    if(!idParam) res.status(400).send("Mauvais identiant.")
    else {
        con.query(`SELECT * FROM Post WHERE topicId=?`,[idParam], (error, result) => {
            if(result.length > 0) res.status(400).send("Impossible de supprimer ce topic, il contient des articles.")
            else connect.query('DELETE FROM Topic WHERE id=?', [idParam], (error, result) => {
                if(error) res.status(400).send(error);
                else res.send(result.message)
            })
        });
    }
});

module.exports = TopicRouter;