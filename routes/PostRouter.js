const express = require("express");
const connect = require("../db");

const PostRouter = express.Router();

PostRouter.route("/").get((req, res) => {
    connect.query('SELECT * FROM Post', (error, results) => {
        res.send(results);
    });
    //Création d'un Post.
}).post((req, res) => {
    // Ajoute une date si elle est manquante
    if(!req.body.date) req.body.date = new Date();
    connect.query('INSERT INTO Post (content, author, date, topicId) VALUES (?, ?, ?, ?)',
    [req.body.content, req.body.author, req.body.date, req.body.topicId],
    (error, result) => {
        if(error) res.status(400).send(error);
        else res.send("Créé.")
    });
});

PostRouter.route("/:id").get((req, res) => {
    const idParam = req.params.id;
    if(!idParam) res.status(400).send("Mauvais identiant.")
    else connect.query('SELECT * FROM Post WHERE id = ? LIMIT 1', [idParam], (error, results) => {
        if(error) res.status(400).send(error);
        else res.send(results[0]);
    });
    //Modificaztion d'un Post.
}).put((req, res) => {
    const idParam = req.params.id;
    if(!idParam) res.status(400).send("Mauvais identiant.")
    else {
        con.query(`UPDATE Post SET content=?, author=?, topicId=? WHERE id=?`,
        [req.body.content, req.body.author, req.body.topicId, idParam], (error, result) => {
            if(error) res.status(400).send(error);
            else res.send(result.message)
        });
    }
// Supression d'un Post.
}).delete((req, res) => {
    const idParam = req.params.id;
    if(!idParam) res.status(400).send("Mauvais identiant.")
    else {
        connect.query('DELETE FROM Post WHERE id=?', [idParam], (error, result) => {
            if(error) res.status(400).send(error);
            else res.send(result.message)
        })
      
    }
});

module.exports = PostRouter;