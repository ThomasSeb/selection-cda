const express = require('express');

const router = express();

const TopicRouter = require('./TopicRouter');
const PostRouter = require('./PostRouter');

router.use('/topics', TopicRouter);
router.use('/posts', PostRouter);

module.exports = router;