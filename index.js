const express =require('express');
const router = require('./routes/routes');
const dotenv = require('dotenv');
const bodyParser = require('body-parser');

dotenv.config();

const port = 8080 || process.env.PORT;
const app = express();

app.use(bodyParser.urlencoded({extende: true}));
app.use(bodyParser.json({limit: '50mb'}));

app.get('/', (req, res) => {
    res.send('Hello!');
})

app.listen(port, () => {
    console.log(`Le serveur est sur le port: ${port}`);
})

app.use("/api", router)