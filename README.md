# Selection CDA



## Installation

- npm i
- Créer une basse de données MySQL (CREATE DATABASE IF NOT EXISTS 'DB_NAME')
- Créer une fichier d'environnement .env comportant:
    - MYSQL_USER = Nom d'utilisateur de la base de données
    - MYSQL_PASSWORD = Mot de passe de la base de données
    - PORT = Port sur lequel fonctionnera l'api, par défaut 8080
    - DB_NAME = Nom de la base de données
- Lancer le serveur node index.js


